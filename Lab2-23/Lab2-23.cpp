/*
1. ��������� ������� ������������ � ������� ��� ���������� ������ � ������� ��� �� ������.
2. ��������� ��������� ����������� ������� � ������������ � ���������, ��������� �������, ��������� ������� �� ������. - ������� ����� ������ �� ������� ���������, �������� ������� ����� 0
3. ��������� ������� ������������ � ������� ��� ��������� ������ � ������� ��� �� ������.
4. ��������� ��������� ���������� ������� � ������������ � ���������, ��������� �������, ��������� ������� �� ������. - ����������� ��� �������� ������ �������
5. ������ � ���������� ������ �������� � ���������� �� � ������������ �� ����� ���������, ��������� �������. - ���������� ��������� ���� � ������
(���������)
1. ������������, ������ � ��������� �������� � ����� �������� � ���� �������. ������� ���������� ��� ��������� �������.
2. ����������� ������� ��� ������������������, �� ����������� ���������� ��� ��������� �������.
3. ������������ �������� ��������� � �������������� ���. � ������� ���������� � �������������, � ������������� �����.
4. ����/����� ����� ������������ � ������� �������:
� char* gets(char*s)

� int puts(char *s)
5. ��� ��������� ����� ������������ ����������� ������� �� ������������� ����� <string.h>
6. ���������� �������� ������������ � ������� ������ �� ������� ������� ����������, ������������� � ������������ ������ �3.
7. ������� main() ������ ��������� ������ �������� ��������/����� � ������ ������� ��� ������������, ������ � ��������� ��������/�����.

*/
#pragma warning(disable:4996) // ������� ����������� "� ����, ��� � ����� � �������, �� ����".
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cstdio>
using namespace std;
const int MAX_SIZE = 100;
void printArray(int *a, int size)
{
	for (int i = 0; i < size; i++)
		cout << a[i] << " ";
	cout << endl;
}
void fillArray(int * a, int size)
{
	for (int i = 0; i < size; i++)
		a[i] = rand() % 10;

}
int* createZerosArray(int * a, int &size)
{
	int *aOdd = new int[MAX_SIZE];
	int j = 0;
	for (int i = 0; i < size; i ++)
	{
		if(a[i] == 0)aOdd[j] = i;
		j++;
	}
	size = j;
	return  aOdd;
}
void printMatrix(int ** a, int size)
{
	for (int j = 0; j < size; j++) {
		for (int i = 0; i < size; i++)
			cout << a[i][j] << " ";
		cout << endl;
	}
}
void fillMatrix(int ** a, int size)
{
	for (int j = 0; j < size; j++)
		for (int i = 0; i < size; i++)
			a[j][i] = rand() % 10;
}
void shiftMatrix(int **a, int size)
{
	for (int k_f = 1; k_f < size; k_f += 2)
	{
	  reverse(a[k_f][0], a[k_f][size]);
	}
}

int  countWords(char *str, int size)
{
	char* rez = new char[strlen(str)];
	char* rez_1 = rez;
	int i = 0;
	int counter = 0;
	while (i < strlen(str)) 
		if (isupper(str[i])) {
			while (!ispunct(str[i]) && !isspace(str[i])) {
				i++;
			}
			counter++;
		}
	return counter;
}
int main(int argc, char* argv[])
{
	srand(time(0));

	int n, k;
	char* str = new char[256];
	fgets(str, 256, stdin); //5 - gets ������� �� ��������� �11 
	cout << "enter n and k:";
	cin >> n >> k;
	int *a = new int[MAX_SIZE];
	fillArray(a, n); //1
	printArray(a, n); //1
	a = createZerosArray(a, n); //2
	printArray(a, n); //2
	int **m = new int*[MAX_SIZE];
	for (int i = 0; i < MAX_SIZE; i++)
		m[i] = new int[MAX_SIZE]; //3
	cout << endl;
	fillMatrix(m, n); //3
	printMatrix(m, n); //3
	cout << endl;
	shiftMatrix(m, n); //4
	printMatrix(m, n); //4
	int count = countWords(str, 256);
	cout << "Words counted: " << count;
	system("pause.exe");
	return 0;
}