﻿/*
1. Используя функции сформировать с помощью ДСЧ одномерный массив и вывести его на печать.
2. Выполнить обработку одномерного массива в соответствии с вариантом, используя функции, результат вывести на печать. - удалить из массива все элементы с чётными номерами
3. Используя функции сформировать с помощью ДСЧ двумерный массив и вывести его на печать.
4. Выполнить обработку двумерного массива в соответствии с вариантом, используя функции, результат вывести на печать. - все нечётные строки матрицы сдвинуть на к элементов влево.
5. Ввести с клавиатуры строку символов и обработать ее в соответствии со своим вариантом, используя функции.
(методичка)
1. Формирование, печать и обработку массивов и строк оформить в виде функции. Массивы передавать как параметры функций.
2. Реализовать массивы как псевдодинамические, их размерности передавать как параметры функций.
3. Формирование массивов выполнить с использованием ДСЧ. В массивы записывать и положительные, и отрицательные числа.
4. Ввод/вывод строк организовать с помощью функций:
· char* gets(char*s)

· int puts(char *s)
5. Для обработки строк использовать стандартные функции из библиотечного файла <string.h>
6. Сортировку массивов организовать с помощью одного из простых методов сортировки, рассмотренных в лабораторной работе №3.
7. Функция main() должна содержать только описание массивов/строк и вызовы функций для формирования, печати и обработки массивов/строк.

 */
#pragma warning(disable:4996) // Говорим компилятору "Я знаю, что я делаю с памятью, не лезь".
#define _CRT_SECURE_NO_WARNINGS
#define _SCL_SECURE_NO_WARNINGS
#include <cstdlib>
#include <ctime>
#include <iostream>
#include <cstdio>
using namespace std;
const int MAX_SIZE = 100;
void printArray(int *a,int size)
{
	for (int i = 0; i < size; i++)
		cout << a[i] << " " ;
	cout << endl;
}
void fillArray(int * a, int size)
{
	for (int i = 0; i < size; i++)
		a[i] = rand() % 10;
	
}
int* removeFromArray(int * a, int &size)
{
	int *aOdd = new int[MAX_SIZE];
	int j = 0;
	for (int i = 0; i < size; i+=2)
	{
		aOdd[j] = a[i];
		j++;
	}
	size = j;
	return  aOdd;
}
void printMatrix(int ** a,int size)
{
	for (int j = 0; j < size; j++){
		for (int i = 0; i < size; i++) 
			cout << a[i][j] << " ";
		cout << endl;
		}
}
void fillMatrix(int ** a,int size)
{
	for (int j = 0; j < size; j++)
	for (int i = 0; i < size; i++)
		a[j][i] = rand() % 10;
}
void shiftMatrix(int **a,int size,int k)
{
	for (int k_f = 1; k_f < size; k_f+=2)
	{
		for (int j = 0; j < k; j++)
			for (int i = 0; i < size - 1; i++)
				swap(a[k_f][i], a[k_f][i + 1]);
	}
}
bool isVowel(char c)
{
	const char *arr_lett = "AaEeIiOoUuYy";
	for (int i = 0; i < strlen(arr_lett); i++)
		if (c == arr_lett[i]) return true;
	return false;
}
char * remWords(char *str,int size)
{
	char* rez = new char[strlen(str)];
	char* rez_1 = rez;

	int i = 0;

	while (i < strlen(str)) {
		if (isupper(str[i]) && isVowel(str[i])) {
			while (!ispunct(str[i]) && !isspace(str[i])) {
				i++;
			}
		}
		*rez++ = str[i++];
	}
	*rez++ = '.';
	*rez = 0;
	return rez_1;
}
int main(int argc, char* argv[])
{
	srand(time(0));
	
	int n,k;
	char* str = new char[256];
	fgets(str, 256, stdin); //5 - gets удалена из стандарта С11 
	cout << "enter n and k:";
	cin >> n >> k;
	int *a = new int[MAX_SIZE];
	fillArray(a, n); //1
	printArray(a, n); //1
	a = removeFromArray(a, n); //2
	printArray(a, n); //2
	int **m = new int*[MAX_SIZE];
	for (int i = 0; i < MAX_SIZE; i++)
		m[i] = new int[MAX_SIZE]; //3
	cout << endl;
	fillMatrix(m, n); //3
	printMatrix(m, n); //3
	cout << endl;
	shiftMatrix(m, n, k); //4
	printMatrix(m, n); //4
	str = remWords(str,256);
	puts(str);
	system("pause.exe");
	return 0;
}