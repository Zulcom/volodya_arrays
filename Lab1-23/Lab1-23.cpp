﻿/*
1) Сформировать массив из n элементов с помощью датчика случайных чисел (n задается пользователем с клавиатуры).
2) Распечатать полученный массив.
3) Выполнить удаление указанных элементов из массива. - все чётные элементы
4) Вывести полученный результат.
5) Выполнить добавление указанных элементов в массив. n элементов, начиная с номера к
6) Вывести полученный результат.
7) Выполнить перестановку элементов в массиве. сдвинуть циклически на m элементов вправо
8) Вывести полученный результат.
9) Выполнить поиск указанных в массиве элементов и подсчитать количество сравнений, необходимых для поиска нужного элемента. - элемент, с заданным ключом (значением)
10) Вывести полученный результат.
11) Выполнить сортировку массива указанным методом - простой выбор.
12) Вывести полученный результат.
13) Выполнить поиск указанных элементов в отсортированном массиве и подсчитать количество сравнений, необходимых для поиска нужного элемента.
14) Вывести полученный результат.
2. Формирование массива осуществляется с помощью датчика случайных чисел.
Для этого можно использовать функцию int rand(), которая возвращает псевдослучайное число из диапазона 0..RAND_MAX=32767,
описание функции находится в файле <stdlib.h>.
В массиве должны быть записаны и положительные и отрицательные элементы. Например, оператор a[I]=rand()%100-50;
формирует псевдослучайное число из диапазона [-50;49].
3. Вывод результатов должен выполняться после выполнения каждого задания. Элементы массива рекомендуется выводить в строчку, разделяя их между собой пробелом.
*/
#pragma warning(disable:4996) // Говорим компилятору "Я знаю, что я делаю с памятью, не лезь".

#include <ctime>
#include <cstdlib>
#include <iostream>
using namespace std;

void print(int* a, int size)
{
	for (int i = 0; i < size; i++)
		cout << a[i] << " ";
	cout << endl;
}

void choicesSort(int* a, int size) // сортировка выбором
{
	for (int repeat_counter = 0; repeat_counter < size; repeat_counter++)
		for (int element_counter = repeat_counter + 1; element_counter < size; element_counter++)
			if (a[repeat_counter] > a[element_counter])
				swap(a[repeat_counter], a[element_counter]);
}

int main()
{
	const int MAX_SIZE = 1000;
	int array[MAX_SIZE];
	srand(time(0)); // настройка генератора
	int n, k,m,key;
	cout << "Enter n and k(k<n),m and key :";
	cin >> n >> k >>m >> key;
	for (int i = 0; i < n; i++)
		array[i] = rand() % 100 - 50; // (1)
	print(array, n); //(2)
	
	for (int i = 0; i < n; i++)
	{
		if(abs(array[i]) %2 == 0) //все чётные элементы
		{
			copy(array + i + 1, array + n, array + i); // двигаем все элементы от этого элемента до последнего элемента на одну позицию назад(3)
			if (i < k)k--;
			n--; i = 0; // массив уменьшился		
		}		
	}
	print(array, n); // (4)
	copy(array + k + 1, array + n, array + n + 1); // (5)
	for (int i = k + 1; i < n + 1;i++) array[i] = rand() % 100 - 50;// (5)
	n += (n - k);// массив увеличился
	print(array, n); // (6)
	for (int j = 0; j < n-m; j++)
		for (int i = 0; i < n-1; i++)
			swap(array[i], array[i + 1]); //(7)
	print(array, n); // (8)

	bool isFound = false;
	for (int i = 0; i < n; i++)
		if (array[i] ==  key)// (9)
		{
			cout << "Element equals " << key << " was found at index " << i << " for " << i << " checks" << endl; //(10)
			isFound = true;
		}
	if (!isFound) cout << "Element equals " << key << " wasn't found for " << n << " checks" << endl;// (10 )
	choicesSort(array, n); //(11)
	print(array, n); // (12)
	isFound = false;
	for (int i = 0; i < n; i++)
		if (array[i] == key)// (13)
		{
			cout << "Element equals " << key << " was found at index " << i << " for " << i << " checks" << endl; //(14)
			isFound = true;
		}
	if (!isFound) cout << "Element equals " <<key << " wasn't found for " << n << " checks" << endl;// (14)

	system("pause.exe");
	return 0;
}
