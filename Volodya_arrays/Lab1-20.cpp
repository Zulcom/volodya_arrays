﻿/* 
1) Сформировать массив из n элементов с помощью датчика случайных чисел (n задается пользователем с клавиатуры). 
2) Распечатать полученный массив. 
3) Выполнить удаление указанных элементов из массива. - минимальный элемент
4) Вывести полученный результат. 
5) Выполнить добавление указанных элементов в массив. элемент с номером к
6) Вывести полученный результат. 
7) Выполнить перестановку элементов в массиве. swap минимальный и максимальный
8) Вывести полученный результат. 
9) Выполнить поиск указанных в массиве элементов и подсчитать количество сравнений, необходимых для поиска нужного элемента. - элемент, равный среднему арифметическому элементов массива
10) Вывести полученный результат. 
11) Выполнить сортировку массива указанным методом - простой выбор.
12) Вывести полученный результат. 
13) Выполнить поиск указанных элементов в отсортированном массиве и подсчитать количество сравнений, необходимых для поиска нужного элемента. 
14) Вывести полученный результат.
2. Формирование массива осуществляется с помощью датчика случайных чисел. 
Для этого можно использовать функцию int rand(), которая возвращает псевдослучайное число из диапазона 0..RAND_MAX=32767, 
описание функции находится в файле <stdlib.h>.
В массиве должны быть записаны и положительные и отрицательные элементы. Например, оператор a[I]=rand()%100-50; 
формирует псевдослучайное число из диапазона [-50;49]. 
3. Вывод результатов должен выполняться после выполнения каждого задания. Элементы массива рекомендуется выводить в строчку, разделяя их между собой пробелом.
 */
#pragma warning(disable:4996) // Говорим компилятору "Я знаю, что я делаю с памятью, не лезь".

#include <ctime>
#include <cstdlib>
#include <iostream>
using namespace std;

void print(int* a, int size)
{
	for (int i = 0; i < size; i++)
		cout << a[i] << " ";
	cout << endl;
}

void choicesSort(int* a, int size) // сортировка выбором
{
	for (int repeat_counter = 0; repeat_counter < size; repeat_counter++)
		for (int element_counter = repeat_counter + 1; element_counter < size; element_counter++)
			if (a[repeat_counter] > a[element_counter])
				swap(a[repeat_counter], a[element_counter]);
}

int main()
{
	const int MAX_SIZE = 1000;
	int array[MAX_SIZE];
	srand(time(0)); // настройка генератора
	int n, k;
	cout << "Enter n and k where k<n:";
	cin >> n >> k;
	for (int i = 0; i < n; i++)
		array[i] = rand() % 100 - 50; // (1)
	print(array, n); //(2)
	int min = 50, minindex = -1;
	for (int i = 0; i < n; i++)
		if (array[i] < min)
		{
			min = array[i];
			minindex = i;
		}
	copy(array + minindex + 1, array + n, array + minindex);// двигаем все элементы от минимального элемента до последнего элемента на одну позицию назад (3)
	n--; // массив уменьшился
	print(array, n); // (4)
	array[n] = array[k]; // добавляем в конец. (5)
	n++; // массив увеличился
	print(array, n); //(6)
	min = 50, minindex = -1;
	int max = -51, maxindex = -1;
	for (int i = 0; i < n; i++)
	{
		if (array[i] < min)
		{
			min = array[i];
			minindex = i;
		}
		if (array[i] > max)
		{
			max = array[i];
			maxindex = i;
		}
	}
	swap(array[minindex], array[maxindex]); // (7)
	print(array, n); //(8) размер не изменился
	int sum = 0;
	for (int i = 0; i < n; i++)
		sum += array[i];
	bool isFound = false;
	for (int i = 0; i < n; i++)
		if (array[i] == sum / n)// (9)
		{
			cout << "Element equals " << sum / n << " was found at index " << i << " for " << i << " checks" << endl; //(10)
			isFound = true;
		}
	if (!isFound) cout << "Element equals " << sum / n << " wasn't found for " << n << " checks" << endl;// (10 если не найден)
	choicesSort(array, n); //(11)
	print(array, n); // (12)
	isFound = false;
	for (int i = 0; i < n; i++)	
		if (array[i] == sum / n)// (13)
		{
			cout << "Element equals " << sum / n << " was found at index " << i << " for " << i << " checks" << endl; //(14)
			isFound = true;
		}
	if (!isFound) cout << "Element equals " << sum / n << " wasn't found for " << n << " checks" << endl;// (14 если не найден)
	system("pause.exe");
	return 0;
}
